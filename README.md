# Running a Permission Node

## Basic Operation
To run a Permission node, simply clone this repo, and build and run
the Docker image with the following commands:

```bash
docker build -t permission-node .
docker run permission-node
```

## Advanced
### Mainnet and Testnet
By default, this repo's Dockerfile will create a container that runs a
Permission node on Mainnet. To run a node on Testnet, update the
Dockerfile's "blockchain_network" ARG to "testnet", like so:

```bash
# Which Blockchain Network
ARG blockchain_network=testnet
```